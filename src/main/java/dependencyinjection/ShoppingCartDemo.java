/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dependencyinjection;


public class ShoppingCartDemo {
    public static void main(String[] args){
        
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceFactory.PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentService(PaymentServiceFactory.PaymentServiceType.DEBIT);
        
        Cart cart = new Cart();
        cart.addProduct(new Product("shirt", 50 ));
        cart.addProduct(new Product("pants", 60));
        
        cart.setPaymentService(creditService);
        cart.payCart();
        
         cart.setPaymentService(debitService);
         cart.payCart();
        
    }
}
