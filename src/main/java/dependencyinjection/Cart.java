/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dependencyinjection;

import java.util.ArrayList;


public class Cart {
    
   ArrayList<Product> products;
   private PaymentService service;

  public void setPaymentService(PaymentService service){
      this.service = service;
  }
  
  public void addProduct(Product product){
   products.add(product);
  }

    public PaymentService getService() {
        return service;
    }
  
  
  public void payCart(){
      int sum=0;
      if(service!=null){
          for(Product x : products){
             sum+= x.getPrice();
          }
            System.out.println("Processing credit payment of " + sum);
      }
      else{
          for(Product x : products){
             sum+= x.getPrice();
          }
            System.out.println("Processing debit payment of " + sum);
      }
          
  }
    
    
}
