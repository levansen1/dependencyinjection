/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dependencyinjection;


public class PaymentServiceFactory {
    private static PaymentServiceFactory obj;
    
    
    
    enum PaymentServiceType{
    CREDIT,
    DEBIT
}

    
    private PaymentServiceFactory(){
        
    }
    
    public PaymentService getPaymentService(PaymentServiceType type){
        if(type==PaymentServiceType.CREDIT){
             return new CreditPaymentService();
        }
  
    return new DebitPaymentService();
  }
    
    public static PaymentServiceFactory getInstance(){
        if(obj == null){
            obj = new PaymentServiceFactory();
        }
        return obj;
    }
}
