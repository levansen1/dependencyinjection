/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dependencyinjection;


public class CreditPaymentService extends PaymentService{
     public void ProcessPayment(double amount){
        System.out.println("Processing credit payment of " + amount);
    }
}
